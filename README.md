# How to
The VR Inventory Example project contains a number of example showcasing how to use the VR Inventory Plugin. You can clone the project by using the clone button or simply download it in its entirety by clicking the download button. The download button is the small arrow pointing down that you will find next to the clone button.

To use the example you must have purchased the VR Inventory Plugin and have it installed in your engine. The example is built in 4.25 but can be easily converted to 4.26 if needed. All the examples are contained inside the ***InventoryExampleMap*** you can find under ***/Content/Levels/***.

# Report issues
If you find any issues, bugs or want to report something wrong with both the VR Inventory Example and the plugin use the issues tab on the left side.
